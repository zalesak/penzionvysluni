$(document).ready(function () {
  $(".popup").each(function () {
    $(this).magnificPopup({
      delegate: ".popup__item",
      type: "image",
      gallery: {
        enabled: true,
      },
    });
  });

  menuLinks = document.querySelector(".navigation>li.has-child>a");
  menuLinks.addEventListener("click", function (e) {
      this.parentNode.classList.toggle("open");
      e.preventDefault;
    }
  );

  $(".lang-selector__button").on("click", function (e) {
    $(".lang-selector__dropdown").toggleClass("open");
    e.preventDefault();
  });

  $(".header__toggle").on("click", function (e) {
    $(".header__toggle").toggleClass("open");
    $(".header__nav").toggleClass("open");
    e.preventDefault();
  });

  $(".slick-init").slick({
    dots: false,
    fade: true,
    cssEase: "linear",
  });

  $(".slick-galerie").slick({
    dots: true,
    infinite: true,
  });

  $(".slick-apartmans").slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
});


function slideTo(target) {
  var elmnt = document.querySelector(target);
  elmnt.scrollIntoView({behavior: "smooth"});
}